package com.assessment.xsis.dao;

import com.assessment.xsis.entity.MovieTab;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieDao extends JpaRepository<MovieTab,Integer> {

}
