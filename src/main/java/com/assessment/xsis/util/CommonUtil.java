package com.assessment.xsis.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtil {
    private static final Logger log = LoggerFactory.getLogger(CommonUtil.class);
    private static ObjectMapper objectMapper = null;
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public static ObjectMapper GetDefaultModelMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
            objectMapper.setDateFormat(df);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
        return objectMapper;
    }

    public static String convertDateToString(Date date, String format){
        DateFormat dateFormat = new SimpleDateFormat(format);
        String result = dateFormat.format(date);
        return result;
    }
}
