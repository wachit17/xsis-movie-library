package com.assessment.xsis.service.implement;

import com.assessment.xsis.dao.MovieDao;
import com.assessment.xsis.entity.MovieTab;
import com.assessment.xsis.model.request.MovieRequest;
import com.assessment.xsis.model.response.MovieRes;
import com.assessment.xsis.service.MovieService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Slf4j
@Service
public class MovieServiceImpl implements MovieService {

    private MovieDao movieDao;

    @Autowired
    public MovieServiceImpl(MovieDao movieDao) {
        this.movieDao = movieDao;
    }

    @Override
    public List<MovieRes> getAllMovies() {
        try{
            List<MovieRes> result = new ArrayList<>();
            List<MovieTab> movieTabs = this.movieDao.findAll();
            for (MovieTab movieTab:movieTabs) {
                MovieRes movieRes = new MovieRes(movieTab);
                result.add(movieRes);
            }
            return result;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Get list movie tab failed, error: {} ", e.getMessage());
            throw new RuntimeException("Get list movie tab failed, error: {} "+ e.getMessage());
        }
    }

    @Override
    public MovieRes getById(Integer id) {
        try {
            MovieTab movieTab = this.movieDao.findById(id).orElse(null);
            if (movieTab == null){
                throw new RuntimeException("not found with id "+id);
            }else {
                MovieRes result = new MovieRes(movieTab);
                return result;
            }

        }catch (Exception e){
            e.printStackTrace();
            log.error("Get movie tab by id failed, error: {} ", e.getMessage());
            throw new RuntimeException("Get movie tab by id failed, error: "+ e.getMessage());
        }
    }

    @Override
    public MovieRes create(MovieRequest request) {
        try{
            MovieTab movieTab = new MovieTab(request);
            movieTab = this.movieDao.save(movieTab);
            MovieRes result = new MovieRes(movieTab);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Create movie tab by id failed, error: {} ", e.getMessage());
            throw new RuntimeException("Create movie tab by id failed, error: {} "+ e.getMessage());
        }
    }

    @Override
    public MovieRes update(MovieRequest request,Integer id) {
        try{
            MovieTab movieTab = this.movieDao.findById(id).orElse(null);
            if (movieTab == null){
                throw new RuntimeException("not found with id "+id);
            }else {
                movieTab = new MovieTab(movieTab,request);
                movieTab = this.movieDao.save(movieTab);
                MovieRes result = new MovieRes(movieTab);
                return result;
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("Update movie tab failed, error: {} ", e.getMessage());
            throw new RuntimeException("Update movie tab failed, error: {} "+ e.getMessage());
        }
    }

    @Override
    public String deleteBy(Integer id) {
        try{
            this.movieDao.deleteById(id);
            return "succes delete movie with id "+id;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Delete movie tab failed, error: {} ", e.getMessage());
            throw new RuntimeException("Update movie tab failed, error: {} "+ e.getMessage());
        }
    }
}
