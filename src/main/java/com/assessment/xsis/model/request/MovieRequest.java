package com.assessment.xsis.model.request;

import com.assessment.xsis.util.CommonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieRequest {
    private String title;
    private String description;
    private Float rating;
    private String image;

    public MovieRequest() {
    }


    @Override
    public String toString() {
        String json;
        try {
            json = CommonUtil.GetDefaultModelMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            json = null;
        }
        return json;
    }
}
