package com.assessment.xsis.entity;

import com.assessment.xsis.model.request.MovieRequest;
import com.assessment.xsis.util.CommonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "movie_tab")
public class MovieTab extends BaseModel{
    @Column(name = "title", length = 250)
    private String title;
    @Column(name = "description", length=2000)
    private String description;
    @Column(name = "rating")
    private Float rating;
    @Column(name = "image")
    private String image;

    public MovieTab() {
    }

    public MovieTab(MovieRequest movieRequest) {
        this.title = movieRequest.getTitle();
        this.description = movieRequest.getDescription();
        this.rating = movieRequest.getRating();
        this.image = movieRequest.getImage();
    }

    public MovieTab(MovieTab movieTab,MovieRequest movieRequest) {
        this.setId(movieTab.getId());
        this.setCreatedAt(movieTab.getCreatedAt());
        this.title = movieRequest.getTitle();
        this.description = movieRequest.getDescription();
        this.rating = movieRequest.getRating();
        this.image = movieRequest.getImage();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        String json;
        try {
            json = CommonUtil.GetDefaultModelMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            json = null;
        }
        return json;
    }
}
