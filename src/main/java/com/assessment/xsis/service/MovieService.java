package com.assessment.xsis.service;

import com.assessment.xsis.model.request.MovieRequest;
import com.assessment.xsis.model.response.MovieRes;

import java.util.List;

public interface MovieService {
    List<MovieRes> getAllMovies();
    MovieRes getById(Integer id);
    MovieRes create(MovieRequest request);
    MovieRes update(MovieRequest request,Integer id);
    String deleteBy(Integer id);
}
