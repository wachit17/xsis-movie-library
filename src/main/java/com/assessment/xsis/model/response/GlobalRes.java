package com.assessment.xsis.model.response;

import com.assessment.xsis.util.CommonUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;

public class  GlobalRes<T> {

    @JsonInclude(JsonInclude.Include.ALWAYS)
    private int status;
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String error;
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String message;

    @JsonInclude(JsonInclude.Include.ALWAYS)
    private T data;

    public GlobalRes() {
    }

    public GlobalRes(T data, int status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
        this.data = data;
    }

    public GlobalRes(int status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
        this.data = null;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        String json;
        try {
            json = CommonUtil.GetDefaultModelMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            json = null;
        }
        return json;
    }
}
