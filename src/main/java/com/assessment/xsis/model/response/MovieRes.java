package com.assessment.xsis.model.response;

import com.assessment.xsis.entity.MovieTab;
import com.assessment.xsis.util.CommonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class MovieRes {
    private Integer id;
    private String title;
    private String description;
    private Float rating;
    private String image;
    private String createdAt;
    private String updatedAt;

    public MovieRes(MovieTab movieTab) {
        this.id = movieTab.getId();
        this.title = movieTab.getTitle();
        this.description = movieTab.getDescription();
        this.rating = movieTab.getRating();
        this.image = movieTab.getImage();
        this.createdAt = movieTab.getCreatedAt()==null?"":CommonUtil.convertDateToString(movieTab.getCreatedAt(),"dd-mm-yyyy HH:mm:ss");
        this.updatedAt = movieTab.getUpdatedAt()==null?"":CommonUtil.convertDateToString(movieTab.getUpdatedAt(),"dd-mm-yyyy HH:mm:ss");
    }

    @Override
    public String toString() {
        String json;
        try {
            json = CommonUtil.GetDefaultModelMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            json = null;
        }
        return json;
    }
}
