package com.assessment.xsis.validator;

import com.assessment.xsis.model.request.MovieRequest;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class MovieRequestValidator implements Validator {
    @Override

    public boolean supports(Class<?> clazz) {
        return MovieRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieRequest request =(MovieRequest) target;
        if (Strings.isEmpty(request.getTitle())){
            errors.rejectValue("title", "title.null", "Title");
        }
        if (Strings.isEmpty(request.getDescription())){
            errors.rejectValue("description", "description.null", "description");
        }
    }
}
