package com.assessment.xsis.controller;

import com.assessment.xsis.model.request.MovieRequest;
import com.assessment.xsis.model.response.GlobalRes;
import com.assessment.xsis.model.response.MovieRes;
import com.assessment.xsis.service.MovieService;
import com.assessment.xsis.validator.MovieRequestValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api")
public class MoviesController {

    private MovieService movieService;
    private MovieRequestValidator movieRequestValidator;

    @Autowired
    public MoviesController(MovieService movieService, MovieRequestValidator movieRequestValidator) {
        this.movieService = movieService;
        this.movieRequestValidator = movieRequestValidator;
    }

    @GetMapping("/Movies")
    public ResponseEntity<Object> getMovies() {
        try {
            List<MovieRes> result = this.movieService.getAllMovies();
            return new ResponseEntity<>(new GlobalRes<>(result, HttpStatus.OK.value(), null,"SUCCESS"), HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new GlobalRes(null, HttpStatus.BAD_REQUEST.value(), e.getMessage(), "FAILED"));
        }
    }
    @GetMapping("/Movies/{id}")
    public ResponseEntity<Object> getMovieDetail(@PathVariable("id") Integer id ) {
        try {
            MovieRes result = this.movieService.getById(id);
            return new ResponseEntity<>(new GlobalRes<>(result, HttpStatus.OK.value(), null,"SUCCESS"), HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new GlobalRes(null, HttpStatus.BAD_REQUEST.value(), e.getMessage(), "FAILED"));
        }
    }
    @PostMapping("/Movies")
    public ResponseEntity<Object> createMovies(@Validated @RequestBody MovieRequest request, BindingResult bindingResult) {
        try {
            this.movieRequestValidator.validate(request,bindingResult);
            if (bindingResult.hasErrors()) {
                List<String> fieldErrors = bindingResult.getFieldErrors().stream().map(item -> item.getDefaultMessage()).collect(Collectors.toList());
                String errorMessages = String.join(", ", fieldErrors);
                throw new RuntimeException(errorMessages+" cannot be empty.");
            }
            MovieRes result = this.movieService.create(request);
            return new ResponseEntity<>(new GlobalRes<>(result, HttpStatus.OK.value(), null,"SUCCESS"), HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new GlobalRes(null, HttpStatus.BAD_REQUEST.value(), e.getMessage(), "FAILED"));
        }
    }
    @PatchMapping("/Movies/{id}")
    public ResponseEntity<Object> updateMovie(@Validated @RequestBody MovieRequest request, BindingResult bindingResult,@PathVariable("id") Integer id) {
        try {
            this.movieRequestValidator.validate(request,bindingResult);
            if (bindingResult.hasErrors()) {
                List<String> fieldErrors = bindingResult.getFieldErrors().stream().map(item -> item.getDefaultMessage()).collect(Collectors.toList());
                String errorMessages = String.join(", ", fieldErrors);
                throw new RuntimeException(errorMessages+" cannot be empty.");
            }
            MovieRes result = this.movieService.update(request,id);
            return new ResponseEntity<>(new GlobalRes<>(result, HttpStatus.OK.value(), null,"SUCCESS"), HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new GlobalRes(null, HttpStatus.BAD_REQUEST.value(), e.getMessage(), "FAILED"));
        }
    }

    @DeleteMapping("/Movies/{id}")
    public ResponseEntity<Object> deleteMovie(@PathVariable("id") Integer id ) {
        try {
            String result = this.movieService.deleteBy(id);
            return new ResponseEntity<>(new GlobalRes<>(result, HttpStatus.OK.value(), null,"SUCCESS"), HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new GlobalRes(null, HttpStatus.BAD_REQUEST.value(), e.getMessage(), "FAILED"));
        }
    }

}
